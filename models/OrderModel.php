<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class OrderModel extends Model {  
        
        protected function getFields(): array{
            return [
                'order_id'          => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'        => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),

                'cart_id'           => new Field((new NumberValidator())->setIntegerLength(11), false),
                'delivery_details'  => new Field((new StringValidator())->setMaxLength(64*1024) ),
                 
                'status'            => Field::readOnlyString(0)                             
            ];
        }

        public function getByStatus(string $status) {
            $sql  = 'SELECT * FROM order WHERE status = ?;';
            $prep = $this->dbc->getDatabaseConnection()->prepare($sql);
            $res  = $prep->execute([$status]);
            $stat = NULL;
            if($res) {
                $stat = $prep->fetch(\PDO::FETCH_OBJ);
            }
            return $stat;
        }
    }