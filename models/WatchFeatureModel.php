<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class WatchFeatureModel extends Model { 
        
        protected function getFields(): array{
            return [
                'watch_feature_id'  => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'        => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                
                'watch_id'          => new Field((new NumberValidator())->setIntegerLength(11) ),
                'feature_id'        => new Field((new NumberValidator())->setIntegerLength(11) )                               
            ];
        }

        public function getByWatchId(int $watchId) {
            return $this->getByFieldName('watch_id', $watchId);
        }
    }