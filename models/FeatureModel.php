<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class FeatureModel extends Model {       

        protected function getFields(): array {
            return [
                'feature_id'    => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'    => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                
                'name'          => new Field((new StringValidator())->setMaxLength(255) )                               
            ];
        }

        public function getByName(string $names) {
            return $this->getByFieldName('name', $names);
        }
    }