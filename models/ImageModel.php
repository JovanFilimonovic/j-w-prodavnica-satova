<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class ImageModel extends Model {

        protected function getFields(): array{
            return [
                'image_id'      => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'    => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                
                'title'         => new Field((new StringValidator())->setMaxLength(128) ),
                'watch_id'      => new Field((new NumberValidator())->setIntegerLength(11) ), 
                'path'          => new Field((new StringValidator())->setMaxLength(128) )                           
            ];
        }
                
        public function getByWatchId(int $watchId) {
            $sql   = 'SELECT * FROM image WHERE watch_id = ?;';
            $prep  = $this->getDatabaseConnection()->prepare($sql);
            $res   = $prep->execute([$watchId]);
            $watch = NULL;
            if($res) {
                $watch = $prep->fetch(\PDO::FETCH_OBJ);
            }
            return $watch;
        }
    }