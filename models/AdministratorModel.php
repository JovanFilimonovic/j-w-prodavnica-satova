<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class AdministratorModel extends Model {    
        
        protected function getFields(): array {
            return [
                'administrator_id'  => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'        => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                
                'email'             => new Field((new StringValidator())->setMaxLength(128) ), 
                'password'          => new Field((new StringValidator())->setMaxLength(128) ), 
                'is_active'         => new Field(new BitValidator() )          
            ];
        }

        public function getByEmail(string $emails) {
            $sql   = 'SELECT * FROM administrator WHERE email = ?;';
            $prep  = $this->getDatabaseConnection()->prepare($sql);
            $res   = $prep->execute([$emails]);
            $email = NULL;
            if($res) {
                $email = $prep->fetch(\PDO::FETCH_OBJ);
            }
            return $email;
        }
    }