<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class AdministratorLoginModel extends Model {  
        
        protected function getFields(): array {
            return [
                'administrator_login_id'    => new Field((new NumberValidator())->setIntegerLength(11), false),
                'logged_at'                 => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),

                'administrator_id'          => new Field((new NumberValidator())->setIntegerLength(11) ),  
                'ip_address'                => new Field((new StringValidator())->setMaxLength(32) )          
            ];
        }

        public function getByIp(string $ipAdress) {
            $sql  = 'SELECT * FROM administrator_login WHERE ip_address = ?;';
            $prep = $this->getDatabaseConnection()->prepare($sql);
            $res  = $prep->execute([$ipAdress]);
            $ip   = NULL;
            if($res) {
                $ip = $prep->fetch(\PDO::FETCH_OBJ);
            }
            return $ip;
        }
    }