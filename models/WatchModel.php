<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class WatchModel extends Model {   

        protected function getFields(): array {
            return [
                'watch_id'          => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'        => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                
                'title'             => new Field((new StringValidator())->setMaxLength(128) ),
                'description'       => new Field((new StringValidator())->setMaxLength(64*1024) ),
                'administrator_id'  => new Field((new NumberValidator())->setIntegerLength(11) ),
                'price'             => new Field((new NumberValidator())->setDecimal()
                                                                        ->setUnsigned()
                                                                        ->setIntegerLength(7)
                                                                        ->setMaxDecimalDigits(2) )               
            ];
        }

        public function getByTitle(string $title) {
            return $this->getByFieldName('title', $title);
        }            
    }