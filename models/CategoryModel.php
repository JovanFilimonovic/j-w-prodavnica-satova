<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class CategoryModel extends Model {  
        
        protected function getFields(): array{
            return [
                'category_id'   => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'    => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                               
                'name'          => new Field((new StringValidator())->setMaxLength(64) )               
            ];
        }

        public function getIdByName(string $name) {
            $sql      = 'SELECT id FROM category WHERE name = ?;';
            $prep     = $this->getDatabaseConnection()->prepare($sql);
            $res      = $prep->execute([$name]);
            $category = NULL;
            if($res) {
                $category = $prep->fetch(\PDO::FETCH_OBJ);
            }
            return $category;
        }
    }