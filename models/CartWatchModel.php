<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class CartWatchLoginModel extends Model {   
        
        protected function getFields(): array {
            return [
                'cart_watch_id' => new Field((new NumberValidator())->setIntegerLength(11), false),
                'added_at'      => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),

                'cart_id'       => new Field((new NumberValidator())->setIntegerLength(11) ),
                'watch_id'      => new Field((new NumberValidator())->setIntegerLength(11) ),
                'amount'        => new Field((new NumberValidator())->setIntegerLength(11) )                          
            ];
        }

        public function getByIp(int $amounts) {
            $sql    = 'SELECT * FROM cart_watch WHERE amount = ?;';
            $prep   = $this->getDatabaseConnection()->prepare($sql);
            $res    = $prep->execute([$amounts]);
            $amount = NULL;
            if($res) {
                $amount = $prep->fetch(\PDO::FETCH_OBJ);
            }
            return $amount;
        }
    }