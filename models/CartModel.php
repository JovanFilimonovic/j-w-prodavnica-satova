<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;

    class CartModel extends Model {   
        
        protected function getFields(): array {
            return [
                'cart_id'           => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'        => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                
                'session_number'    => new Field((new StringValidator())->setMaxLength(16) )              
            ];
        }
       
    }