<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class WatchCategoryModel extends Model {    

        protected function getFields(): array{
            return [
                'watch_category_id' => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'        => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                
                'watch_id'          => new Field((new NumberValidator())->setIntegerLength(11) ),
                'category_id'       => new Field((new NumberValidator())->setIntegerLength(11) )                             
            ];
        }

        public function getWatchIdByCategoryId(int $categoryId): array {
            $sql  = 'SELECT watch_id FROM watch_category WHERE category_id = ?;';
            $prep = $this->getDatabaseConnection()->prepare($sql);
            $res  = $prep->execute([$categoryId]);
            $watchIds = [];
            if($res) {
                $watchIds = $prep->fetchAll(\PDO::FETCH_OBJ);
            }
            return $watchIds;
        }

        public function getByWatchId(int $watchId) {
            return $this->getByFieldName('watch_id', $watchId);
        }
        
    }