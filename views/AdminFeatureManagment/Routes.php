<?php
    return [
        App\Core\Route::get('|^admin/register/?$|',                     'Main',                     'getRegister'),
        App\Core\Route::post('|^admin/register/?$|',                    'Main',                     'postRegister'),
        App\Core\Route::get('|^admin/login/?$|',                        'Main',                     'getLogin'),
        App\Core\Route::post('|^admin/login/?$|',                       'Main',                     'postLogin'),

        App\Core\Route::get('|^category/([0-9]+)/?$|',                  'Category',                 'show'),
       
        App\Core\Route::get('|^watch/([0-9]+)/?$|',                     'Watch',                    'show'),

        # Admin role routes:
        App\Core\Route::get('|^admin/dashboard/?$|',                    'AdminDashboard',           'index'),

        App\Core\Route::get('|^admin/categories/?$|',                   'AdminCategoryManagment',   'categories'),
        App\Core\Route::get('|^admin/categories/edit/([0-9]+)/?$|',     'AdminCategoryManagment',   'getEdit'),
        App\Core\Route::post('|^admin/categories/edit/([0-9]+)/?$|',    'AdminCategoryManagment',   'postEdit'),
        App\Core\Route::get('|^admin/categories/add/?$|',               'AdminCategoryManagment',   'getAdd'),
        App\Core\Route::post('|^admin/categories/add/?$|',              'AdminCategoryManagment',   'postAdd'),
        App\Core\Route::get('|^admin/categories/delete/?$|',            'AdminCategoryManagment',   'deleteById'),
        App\Core\Route::post('|^admin/categories/delete/?$|',           'AdminCategoryManagment',   'postDeleteById'),

        App\Core\Route::get('|^admin/watches/?$|',                      'AdminWatchManagment',      'watches'),
        App\Core\Route::get('|^admin/watches/edit/([0-9]+)/?$|',        'AdminWatchManagment',      'getEdit'),
        App\Core\Route::post('|^admin/watches/edit/([0-9]+)/?$|',       'AdminWatchManagment',      'postEdit'),
        App\Core\Route::get('|^admin/watches/add/?$|',                  'AdminWatchManagment',      'getAdd'),
        App\Core\Route::post('|^admin/watches/add/?$|',                 'AdminWatchManagment',      'postAdd'),
        
        App\Core\Route::get('|^admin/features/?$|',                      'AdminFeatureManagment',   'features'),
        App\Core\Route::get('|^admin/features/edit/([0-9]+)/?$|',        'AdminFeatureManagment',   'getEdit'),
        App\Core\Route::post('|^admin/features/edit/([0-9]+)/?$|',       'AdminFeatureManagment',   'postEdit'),
        App\Core\Route::get('|^admin/features/add/?$|',                  'AdminFeatureManagment',   'getAdd'),
        App\Core\Route::post('|^admin/features/add/?$|',                 'AdminFeatureManagment',   'postAdd'),
        App\Core\Route::get('|^admin/features/delete/?$|',               'AdminFeatureManagment',   'deleteById'),
        App\Core\Route::post('|^admin/features/delete/?$|',              'AdminFeatureManagment',   'postDeleteById'),        

        App\Core\Route::get('|^admin/logs/?$|',                          'AdminLogs',               'show'),
        
        App\Core\Route::get('|^admin/orders/?$|',                      'AdminOrderManagment',   'orders'),
        App\Core\Route::get('|^admin/orders/edit/([0-9]+)/?$|',        'AdminOrderManagment',   'getEdit'),
        App\Core\Route::post('|^admin/orders/edit/([0-9]+)/?$|',       'AdminOrderManagment',   'postEdit'),
        
        App\Core\Route::any('|^.*$|',                                   'Main',                     'home')
    ];