<?php
    namespace App\Controllers;
    use App\Models\FeatureModel;

    class AdminFeatureManagmentController extends \App\Core\Role\UserRoleController {
        public function features() {
            $featureModel = new FeatureModel($this->getDatabaseConnection());
            $features     = $featureModel->getAll();
            $this->set('features', $features);
        }

        public function getEdit($featureId) {
            $featureModel = new FeatureModel($this->getDatabaseConnection());
            $feature      = $featureModel->getById($featureId);

            if (!$feature) {
                $this->redirect(\Configuration::BASE . 'admin/features');
            }

            $this->set('feature', $feature);

            return $featureModel;
        }

        public function postEdit($featureId) {
            $featureModel = $this->getEdit($featureId);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $featureModel->editById($featureId, [
                'name' => $name
            ]);

            $this->redirect(\Configuration::BASE . 'admin/features');
        }

        public function getAdd() {

        }

        public function postAdd() {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $featureModel = new \App\Models\FeatureModel($this->getDatabaseConnection());
            
            $featureId = $featureModel->add([
                'name' => $name
            ]);

            if ($featureId) {
                $this->redirect(\Configuration::BASE . 'admin/features');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati ovu kategoriju!');
        }

        public function deleteById(){
            $featureModel = new \App\models\FeatureModel($this->getDatabaseConnection());
            $feature      = $featureModel->getAll();
            $this->set('features',$feature);
        }

        public function postDeleteById(){            
            $feature1     = \filter_input(INPUT_POST,'feature_id', FILTER_SANITIZE_NUMBER_INT);
            $featureModel = new \App\models\FeatureModel($this->getDatabaseConnection());
            $feature      = $featureModel->deleteById($feature1);
            $this->redirect(\Configuration::BASE . 'admin/features');
        }
    }