<?php
    namespace App\Controllers;
    use App\Models\CategoryModel;
    use App\Core\Controller;
    use App\Models\WatchModel;
    use App\Models\WatchCategoryModel;

    class CategoryController extends Controller {
        public function show($id){
            $categoryModel      = new CategoryModel($this->getDatabaseConnection());
            $category           = $categoryModel->getById($id);
            $watchCategoryModel = new WatchCategoryModel($this->getDatabaseConnection());
            $watchModel         = new WatchModel($this->getDatabaseConnection());
            $watchesIds         = $watchCategoryModel->getWatchIdbyCategoryId($id);
            $watchesInCategory  = $watchModel->getById($id); 
            $imageModel         = new \App\Models\ImageModel($this->getDatabaseConnection());

            $images  = [];
            $watches = [];
            foreach ($watchesIds as &$wId) {             
                array_push($watches, $watchModel->getById($wId->watch_id));
                array_push($images, $imageModel->getByWatchId($wId->watch_id));                
            } 

            $this->set('watchesInCategory', $watches);           
            $this->set('category', $category);  
            $this->set('images', $images);
        }
    }