<?php
    namespace App\Controllers;
    
    use App\Core\DatabaseConnection;
    use App\Core\Controller;
    use App\Models\CategoryModel;    

    class MainController extends Controller {
      
        public function home(){
            $categoryModel = new CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function getRegister() {
            
        }

        public function postRegister() {
            $email     = \filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);            
            $password1 = \filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
            $password2 = \filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

            if ($password1 !== $password2) {
                $this->set('registerMessage', 'Doslo je do greške: Niste uneli dva puta istu lozinku.');
                $this->set('registerClass', 'alert-danger');
                $this->set('registerLink', 'admin/register');
                $this->set('registerLinkMessage', 'da se vratite nazad');
                return;
            }

            $validanPassword = (new \App\Validators\StringValidator())
                ->setMinLength(7)
                ->setMaxLength(120)
                ->isValid($password1);

            if ( !$validanPassword) {
                $this->set('registerMessage', 'Doslo je do greške: Lozinka nije ispravnog formata.');
                $this->set('registerClass', 'alert-danger');
                $this->set('registerLink', 'admin/register');
                $this->set('registerLinkMessage', 'da se vratite nazad');
                return;
            }

            $administratorModel = new \App\Models\AdministratorModel($this->getDatabaseConnection());

            $admin = $administratorModel->getByFieldName('email', $email);
            if ($admin) {
                $this->set('registerMessage', 'Doslo je do greške: Već postoji admin sa tom adresom e-pošte.');
                $this->set('registerClass', 'alert-danger');
                $this->set('registerLink', 'admin/register');
                $this->set('registerLinkMessage', 'da se vratite nazad');
                return;
            }

            $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

            $adminId = $administratorModel->add([
                'email'     => $email,
                'password'  => $passwordHash                 
            ]);

            if (!$adminId) {
                $this->set('registerMessage', 'Doslo je do greške: Nije bilo uspešno registrovanje naloga.');
                $this->set('registerClass', 'alert-danger');
                $this->set('registerLink', 'admin/register');
                return;
            }

            $this->set('registerMessage', 'Napravljen je novi nalog. Sada možete da se prijavite.');
            $this->set('registerClass', 'alert-success');
            $this->set('registerLink', 'admin/login');
            $this->set('registerLinkMessage', 'da se ulogujete');
        }

        public function getLogin() {

        }

        public function postLogin() {        
            $email = \filter_input(INPUT_POST, 'login_email', FILTER_SANITIZE_STRING);
            $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

            $validanPassword = (new \App\Validators\StringValidator())
                ->setMinLength(7)
                ->setMaxLength(120)
                ->isValid($password);

            if ( !$validanPassword) {
                $this->set('message', 'Doslo je do greške: Lozinka nije ispravnog formata.');
                return;
            }

            $administratorModel = new \App\Models\AdministratorModel($this->getDatabaseConnection());

            $admin = $administratorModel->getByFieldName('email', $email);
            if (!$admin) {
                $this->set('message', 'Doslo je do greške: Ne postoji admin sa tom adresom e-pošte.');
                return;
            }

            if (!password_verify($password, $admin->password)) {
                sleep(1);
                $this->set('message', 'Doslo je do greške: Lozinka nije ispravna.');
                return;
            }

            $this->getSession()->put('administrator_id', $admin->administrator_id);
            $this->getSession()->save();

            $administratorLoginModel = new \App\Models\AdministratorLoginModel($this->getDatabaseConnection());
            $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');

            $adminLogin = $administratorLoginModel->add([
                'administrator_id'  => $admin->administrator_id,
                'ip_address'        => $ipAddress                 
            ]);

            $this->redirect(\Configuration::BASE . 'admin/dashboard');
        }            

        public function getLogout() {
            $this->getSession()->remove('administrator_id');
            $this->getSession()->save();
            $this->redirect(\Configuration::BASE);
        }
    }