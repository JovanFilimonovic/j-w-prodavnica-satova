<?php
    namespace App\Controllers;
    use App\Models\CategoryModel;

    class AdminCategoryManagmentController extends \App\Core\Role\UserRoleController {
        public function categories() {
            $categoryModel = new CategoryModel($this->getDatabaseConnection());
            $categories    = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function getEdit($categoryId) {
            $categoryModel = new CategoryModel($this->getDatabaseConnection());
            $category      = $categoryModel->getById($categoryId);

            if (!$category) {
                $this->redirect(\Configuration::BASE . 'admin/categories');
            }

            $this->set('category', $category);

            return $categoryModel;
        }

        public function postEdit($categoryId) {
            $categoryModel = $this->getEdit($categoryId);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $categoryModel->editById($categoryId, [
                'name' => $name
            ]);

            $this->redirect(\Configuration::BASE . 'admin/categories');
        }

        public function getAdd() {

        }

        public function postAdd() {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            
            $categoryId = $categoryModel->add([
                'name'  => $name
            ]);

            if ($categoryId) {
                $this->redirect(\Configuration::BASE . 'admin/categories');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati ovu kategoriju!');
        }

        public function deleteById(){
            $categoryModel = new \App\models\CategoryModel($this->getDatabaseConnection());
            $category      = $categoryModel->getAll();
            $this->set('categories',$category);
        }

        public function postDeleteById(){            
            $category1     = \filter_input(INPUT_POST,'category_id', FILTER_SANITIZE_NUMBER_INT);
            $categoryModel = new \App\models\CategoryModel($this->getDatabaseConnection());
            $category      = $categoryModel->deleteById($category1);
            $this->redirect(\Configuration::BASE . 'admin/categories');
        }
    }