<?php
    namespace App\Controllers;
    use App\Models\OrderModel;

    class AdminOrderManagmentController extends \App\Core\Role\UserRoleController {
        public function orders() {
            $orderModel = new OrderModel($this->getDatabaseConnection());
            $orders = $orderModel->getAll();
            $this->set('orders', $orders);
        }
    }