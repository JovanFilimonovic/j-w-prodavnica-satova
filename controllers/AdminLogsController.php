<?php
    namespace App\Controllers;
    use App\Models\AdministratorLoginModel;

    class AdminLogsController extends \App\Core\Role\UserRoleController {
        public function show() {
            $administratorLoginModel = new AdministratorLoginModel($this->getDatabaseConnection());
            $adminLogs = $administratorLoginModel->getAll();
            $this->set('logs', $adminLogs);
        }
    }