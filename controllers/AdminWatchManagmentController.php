<?php
    namespace App\Controllers;
    use App\Models\WatchModel;

    class AdminWatchManagmentController extends \App\Core\Role\UserRoleController {
        public function watches() {
            $watchModel = new WatchModel($this->getDatabaseConnection());
            $watches = $watchModel->getAll();
            $this->set('watches', $watches);
        }

        public function getEdit($watchId) {       
            $watchModel = new WatchModel($this->getDatabaseConnection());
            $watch  = $watchModel->getById($watchId);

            if (!$watch) {
                $this->redirect(\Configuration::BASE . 'admin/watches');
            }

            $categoryModel      = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $featureModel       = new \App\Models\FeatureModel($this->getDatabaseConnection());
            $imageModel         = new \App\Models\ImageModel($this->getDatabaseConnection());
            $watchCategoryModel = new \App\Models\WatchCategoryModel($this->getDatabaseConnection());
            $watchFeatureModel  = new \App\Models\WatchFeatureModel($this->getDatabaseConnection());

            $categories = $categoryModel->getAll();
            $features   = $featureModel->getAll(); 
            $images     = $imageModel->getAll();

            $this->set('categories', $categories);
            $this->set('features', $features);
            $this->set('watch', $watch);

            $watchCategory  = $watchCategoryModel->getByWatchId($watchId);
            $watchFeature   = $watchFeatureModel->getByWatchId($watchId);
            $image          = $imageModel->getByWatchId($watchId);

            $this->set('selectedCategory', $watchCategory->category_id);
            $this->set('selectedFeature', $watchCategory->category_id);
            $this->set('image', $image);

            return $watchModel;
        }       
        
        public function postEdit($watchId) {
            $watchModel  = $this->getEdit($watchId);

            $title       = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price       = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
            $category_id = filter_input(INPUT_POST, 'category', FILTER_SANITIZE_NUMBER_INT);
            $feature_id  = filter_input(INPUT_POST, 'feature', FILTER_SANITIZE_NUMBER_INT);

            $watchModel         = new \App\Models\WatchModel($this->getDatabaseConnection());            
            $imageModel         = new \App\Models\ImageModel($this->getDatabaseConnection());
            $watchCategoryModel = new \App\Models\WatchCategoryModel($this->getDatabaseConnection());
            $watchFeatureModel  = new \App\Models\WatchFeatureModel($this->getDatabaseConnection());

            $watchCategory = $watchCategoryModel->getByWatchId($watchId);
            $watchFeature  = $watchFeatureModel->getByWatchId($watchId);
            
            if (!$feature_id || !$category_id) {
                $this->set('message', 'Doslo je do greske: prazne vrednosti za category i feature!');
                return;
            }

            $watchEdit = $watchModel->editById($watchId, [
                'title'             => $title,
                'description'       => $description,                
                'price'             => $price
            ]);

            if (!$watchEdit) {
                $this->set('message', 'Doslo je do greske: Nije moguce dodati ovaj sat!');
                return;
            }            

            $watchCategoryEdit = $watchCategoryModel->editById($watchCategory->watch_category_id,[                
                'category_id'   => $category_id                
            ]);

            if (!$watchCategoryEdit) {
                $this->set('message', 'Doslo je do greske: Nije moguce staviti satu zeljenu kategoriju!');
                return;
            }

            $watchFeatureEdit = $watchFeatureModel->editById($watchFeature->watch_feature_id,[                
                'feature_id'    => $feature_id                
            ]);

            if (!$watchFeatureEdit) {
                $this->set('message', 'Doslo je do greske: Nije moguce staviti satu zeljenu karakteristiku!');
                return;
            }
            
            $this->redirect(\Configuration::BASE . 'admin/watches');
        }

        public function deleteById(){
            $watchModel = new \App\models\WatchModel($this->getDatabaseConnection());
            $watch = $watchModel->getAll();
            $this->set('watches',$watch);
        }

        public function postDeleteById(){            
            $watch1             = \filter_input(INPUT_POST,'watch_id', FILTER_SANITIZE_NUMBER_INT);
            $watchModel         = new \App\models\WatchModel($this->getDatabaseConnection());
            $watchCategoryModel = new \App\Models\WatchCategoryModel($this->getDatabaseConnection());
            $watchFeatureModel  = new \App\Models\WatchFeatureModel($this->getDatabaseConnection()); 

            $watchCategoryDelete = $watchCategoryModel->deleteByWatchId($watch1);

            if (!$watchCategoryDelete) {
                $this->set('message', 'Doslo je do greske: Nije moguce obrisati sat');
                return;
            }
            $watchFeatureDelete = $watchFeatureModel->deleteByWatchId($watch1);

            if (!$watchFeatureDelete) {
                $this->set('message', 'Doslo je do greske: Nije moguce obrisati sat');
                return;
            }

            $watchDelete = $watchModel->deleteByWatchId($watch1);

            if (!$watchDelete) {
                $this->set('message', 'Doslo je do greske: Nije moguce obrisati sat');
                return;
            }
            
            $this->redirect(\Configuration::BASE . 'admin/watches');
        }

        public function getAdd() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $featureModel  = new \App\Models\FeatureModel($this->getDatabaseConnection());

            $categories = $categoryModel->getAll();
            $features   = $featureModel->getAll(); 

            $this->set('categories', $categories);
            $this->set('features', $features);
        }

        public function postAdd() {
            $title       = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price       = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
            $category_id = filter_input(INPUT_POST, 'category', FILTER_SANITIZE_NUMBER_INT);
            $feature_id  = filter_input(INPUT_POST, 'feature', FILTER_SANITIZE_NUMBER_INT);

            $watchModel         = new \App\Models\WatchModel($this->getDatabaseConnection());            
            $imageModel         = new \App\Models\ImageModel($this->getDatabaseConnection());
            $watchCategoryModel = new \App\Models\WatchCategoryModel($this->getDatabaseConnection());
            $watchFeatureModel  = new \App\Models\WatchFeatureModel($this->getDatabaseConnection());
            
            if (!$feature_id || !$category_id) {
                $this->set('message', 'Doslo je do greske: prazne vrednosti za category i feature!');
                return;
            }

            $watchId = $watchModel->add([
                'title'             => $title,
                'description'       => $description,
                'administrator_id'  => $this->getSession()->get('administrator_id'),
                'price'             => $price
            ]);

            if (!$watchId) {
                $this->set('message', 'Doslo je do greske: Nije moguce dodati ovaj sat!');
                return;
            }            

            $watchCategoryId = $watchCategoryModel->add([
                'watch_id'      => $watchId,
                'category_id'   => $category_id                
            ]);

            if (!$watchCategoryId) {
                $this->set('message', 'Doslo je do greske: Nije moguce staviti satu zeljenu kategoriju');
                return;
            }

            $watchFeatureId = $watchFeatureModel->add([
                'watch_id'      => $watchId,
                'feature_id'    => $feature_id                
            ]);

            if (!$watchFeatureId) {
                $this->set('message', 'Doslo je do greske: Nije moguce staviti satu zeljenu feature');
                return;
            }
            
            $this->redirect(\Configuration::BASE . 'admin/watches');            
        }        
    }