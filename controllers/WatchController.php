<?php
    namespace App\Controllers;
    use App\Models\WatchModel;
    use App\Core\Controller;

    class WatchController extends \App\Core\Controller {
        public function show($id) {
            $watchModel         = new \App\Models\WatchModel($this->getDatabaseConnection());            
            $watch              = $watchModel->getById($id);
            $watchCategoryModel = new \App\Models\WatchCategoryModel($this->getDatabaseConnection());   
            $watchCategory      = $watchCategoryModel->getByWatchId($id);     
            $categoryModel      = new \App\Models\CategoryModel($this->getDatabaseConnection());            
            $category           = $categoryModel->getById($watchCategory->category_id);
            $imageModel         = new \App\Models\ImageModel($this->getDatabaseConnection());
            $image              = $imageModel->getByWatchId($id);
            $watchFeatureModel  = new \App\Models\WatchFeatureModel($this->getDatabaseConnection());
            $watchFeature       = $watchFeatureModel->getByWatchId($id);
            $featureModel       = new \App\Models\FeatureModel($this->getDatabaseConnection());
            $feature            = $featureModel->getById($watchFeature->feature_id);         
                  
            $this->set('watch', $watch);   
            $this->set('image', $image); 
            $this->set('feature', $feature);
            $this->set('watchFeature', $watchFeature);
            $this->set('category', $category);                                
        }
    }