/*
 Navicat Premium Data Transfer

 Source Server         : connection_1
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : watch_store

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 04/07/2018 23:29:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator`  (
  `administrator_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`administrator_id`) USING BTREE,
  UNIQUE INDEX `uq_administrator_email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of administrator
-- ----------------------------
INSERT INTO `administrator` VALUES (1, '2018-07-02 15:43:06', 'admin@admin.rs', '$2y$10$BThHCd4PgP23uREuIxD/OupJhOKwLOPBOEj7diBJAw5Ki7q8Z7Rmi', 1);
INSERT INTO `administrator` VALUES (2, '2018-07-04 18:55:14', 'jovan.filimonovic.14@singimail.rs', '$2y$10$LNu2voMymr3Wgge5d1tz1.Jh7WqVK3/E1EoEYl8ISAV95NxU5Omte', 1);
INSERT INTO `administrator` VALUES (3, '2018-07-04 19:00:09', 'milan.rosic.14@singimail.rs', '$2y$10$gRd7Ub8nvlp5hHRV/jhusuO/BsIAQNL2IFc1cVU0HTq.rmr0urNVK', 1);
INSERT INTO `administrator` VALUES (4, '2018-07-04 19:02:22', 'admin@jm.com', '$2y$10$L8CK0lEE3dtj6JMLEzl82OUDnNRpFErxyUgRRksDzUZP0.fgs9AhS', 1);
INSERT INTO `administrator` VALUES (5, '2018-07-04 19:03:29', 'proba@test.com', '$2y$10$ZscWEeKywfbL915IMVxp6ehLezmx4IhKzwbRQ5QPVGFOTuq2jQj2a', 1);

-- ----------------------------
-- Table structure for administrator_login
-- ----------------------------
DROP TABLE IF EXISTS `administrator_login`;
CREATE TABLE `administrator_login`  (
  `administrator_login_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `logged_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `administrator_id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`administrator_login_id`) USING BTREE,
  INDEX `fk_administrator_login_administrator_id`(`administrator_id`) USING BTREE,
  CONSTRAINT `fk_administrator_login_administrator_id` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`administrator_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of administrator_login
-- ----------------------------
INSERT INTO `administrator_login` VALUES (2, '2018-06-06 22:34:42', 3, '::1');
INSERT INTO `administrator_login` VALUES (3, '2018-07-02 23:21:27', 1, '::1');
INSERT INTO `administrator_login` VALUES (4, '2018-07-02 23:29:52', 1, '::1');
INSERT INTO `administrator_login` VALUES (5, '2018-07-02 23:30:59', 1, '::1');
INSERT INTO `administrator_login` VALUES (6, '2018-07-03 17:53:41', 1, '::1');
INSERT INTO `administrator_login` VALUES (7, '2018-07-04 00:17:23', 1, '::1');
INSERT INTO `administrator_login` VALUES (8, '2018-07-04 01:36:53', 3, '::1');
INSERT INTO `administrator_login` VALUES (9, '2018-07-04 05:12:51', 1, '::1');
INSERT INTO `administrator_login` VALUES (10, '2018-07-04 05:23:49', 3, '::1');
INSERT INTO `administrator_login` VALUES (11, '2018-07-04 06:20:46', 1, '::1');
INSERT INTO `administrator_login` VALUES (12, '2018-07-04 18:30:22', 1, '::1');
INSERT INTO `administrator_login` VALUES (13, '2018-07-04 19:04:35', 1, '::1');
INSERT INTO `administrator_login` VALUES (14, '2018-07-04 21:21:12', 2, '::1');

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `session_number` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`cart_id`) USING BTREE,
  UNIQUE INDEX `uq_cart_session_number`(`session_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES (1, '2018-06-05 22:40:27', '555333');
INSERT INTO `cart` VALUES (2, '2018-05-30 22:40:58', '777666');

-- ----------------------------
-- Table structure for cart_watch
-- ----------------------------
DROP TABLE IF EXISTS `cart_watch`;
CREATE TABLE `cart_watch`  (
  `cart_watch_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `added_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cart_id` int(11) UNSIGNED NOT NULL,
  `watch_id` int(11) UNSIGNED NOT NULL,
  `amount` int(11) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`cart_watch_id`) USING BTREE,
  INDEX `fk_cart_watch_cart_id`(`cart_id`) USING BTREE,
  INDEX `fk_cart_watch_watch_id`(`watch_id`) USING BTREE,
  CONSTRAINT `fk_cart_watch_card_id` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_cart_watch_watch_id` FOREIGN KEY (`watch_id`) REFERENCES `watch` (`watch_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cart_watch
-- ----------------------------
INSERT INTO `cart_watch` VALUES (1, '2018-06-05 22:41:31', 1, 1, 1);
INSERT INTO `cart_watch` VALUES (2, '2018-06-05 22:41:40', 2, 2, 3);

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`) USING BTREE,
  UNIQUE INDEX `uq_category_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, '2018-06-15 22:37:04', 'Muški');
INSERT INTO `category` VALUES (2, '2018-06-05 22:37:17', 'Ženski');
INSERT INTO `category` VALUES (3, '2018-07-02 17:46:21', 'Dečiji');
INSERT INTO `category` VALUES (4, '2018-07-03 17:54:00', 'Smartwatch');
INSERT INTO `category` VALUES (5, '2018-07-04 21:39:48', 'Kategorija #5');
INSERT INTO `category` VALUES (6, '2018-07-05 21:40:04', 'Kategorija #6');
INSERT INTO `category` VALUES (7, '2018-07-04 21:40:16', 'Kategorija #7');
INSERT INTO `category` VALUES (8, '2018-07-04 21:40:25', 'Kategorija #8');

-- ----------------------------
-- Table structure for feature
-- ----------------------------
DROP TABLE IF EXISTS `feature`;
CREATE TABLE `feature`  (
  `feature_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`feature_id`) USING BTREE,
  UNIQUE INDEX `uq_feature_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of feature
-- ----------------------------
INSERT INTO `feature` VALUES (1, '2018-06-07 22:37:41', 'Karakteristika #1');
INSERT INTO `feature` VALUES (2, '2018-06-02 22:38:00', 'Karakteristika #2');
INSERT INTO `feature` VALUES (3, '2018-06-04 22:38:09', 'Karakteristika #3');
INSERT INTO `feature` VALUES (4, '2018-07-03 02:53:43', 'Karakteristika #4');
INSERT INTO `feature` VALUES (5, '2018-07-04 21:39:24', 'Karakteristika #5');
INSERT INTO `feature` VALUES (6, '2018-07-04 21:40:58', 'Karakteristika #6');
INSERT INTO `feature` VALUES (7, '2018-07-04 21:41:09', 'Karakteristika #7');
INSERT INTO `feature` VALUES (8, '2018-07-04 22:59:19', 'Karakteristika #8');
INSERT INTO `feature` VALUES (9, '2018-07-04 22:59:24', 'Karakteristika #9');
INSERT INTO `feature` VALUES (10, '2018-07-04 22:59:27', 'Karakteristika #10');

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image`  (
  `image_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `watch_id` int(11) UNSIGNED NOT NULL,
  `path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`image_id`) USING BTREE,
  UNIQUE INDEX `uq_image_path`(`path`) USING BTREE,
  INDEX `fk_image_watch_id`(`watch_id`) USING BTREE,
  CONSTRAINT `fk_image_watch_id` FOREIGN KEY (`watch_id`) REFERENCES `watch` (`watch_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of image
-- ----------------------------
INSERT INTO `image` VALUES (1, '2018-06-12 22:38:32', 'Sat #1', 1, '/assets/img/watches/1.jpg');
INSERT INTO `image` VALUES (2, '2018-06-05 22:39:18', 'Sat #2', 2, '/assets/img/watches/2.jpg');
INSERT INTO `image` VALUES (3, '2018-07-04 22:48:17', 'Sat #3', 3, '/assets/img/watches/3.jpg');
INSERT INTO `image` VALUES (4, '2018-07-04 22:48:29', 'Sat #4', 4, '/assets/img/watches/4.jpg');
INSERT INTO `image` VALUES (5, '2018-07-04 22:51:51', 'Sat #5', 5, '/assets/img/watches/5.jpg');
INSERT INTO `image` VALUES (6, '2018-07-04 22:52:07', 'Sat #6', 6, '/assets/img/watches/6.jpg');
INSERT INTO `image` VALUES (7, '2018-07-04 22:52:22', 'Sat  #7', 7, '/assets/img/watches/7.jpg');
INSERT INTO `image` VALUES (8, '2018-07-04 22:52:38', 'Sat  #8', 8, '/assets/img/watches/8.jpg');
INSERT INTO `image` VALUES (9, '2018-07-04 22:52:50', 'Sat  #9', 9, '/assets/img/watches/9.jpg');
INSERT INTO `image` VALUES (10, '2018-07-04 22:52:59', 'Sat  #10', 10, '/assets/img/watches/10.jpg');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `order_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cart_id` int(11) UNSIGNED NOT NULL,
  `delivery_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','canceled','shipped') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`order_id`) USING BTREE,
  UNIQUE INDEX `uq_order_cart_id`(`cart_id`) USING BTREE,
  CONSTRAINT `fk_order_cart_id` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES (1, '2018-06-05 22:39:50', 1, 'nesto ovde ide', 'shipped');
INSERT INTO `order` VALUES (2, '2018-05-31 22:40:43', 2, 'i ovde nesto pise', 'canceled');

-- ----------------------------
-- Table structure for watch
-- ----------------------------
DROP TABLE IF EXISTS `watch`;
CREATE TABLE `watch`  (
  `watch_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `administrator_id` int(11) UNSIGNED NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  PRIMARY KEY (`watch_id`) USING BTREE,
  INDEX `fk_watch_administrator_id`(`administrator_id`) USING BTREE,
  CONSTRAINT `fk_watch_administrator_id` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`administrator_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of watch
-- ----------------------------
INSERT INTO `watch` VALUES (1, '2018-05-30 22:35:31', 'Sat #1', 'opis za sat #1', 1, 12000.00);
INSERT INTO `watch` VALUES (2, '2018-06-03 22:36:17', 'Sat #2', 'opis za sat #2', 3, 15300.00);
INSERT INTO `watch` VALUES (3, '2018-07-04 06:21:03', 'Sat #3', 'opis za sat #3', 1, 9999.99);
INSERT INTO `watch` VALUES (4, '2018-07-04 21:42:05', 'Sat #4', 'opis za sat #4', 2, 25000.00);
INSERT INTO `watch` VALUES (5, '2018-07-04 21:42:15', 'Sat #5', 'opis za sat #5', 2, 19400.00);
INSERT INTO `watch` VALUES (6, '2018-07-04 21:42:15', 'Sat #6', 'opis za sat #6', 5, 33400.00);
INSERT INTO `watch` VALUES (7, '2018-07-04 21:43:43', 'Sat #7', 'opis za sat #7', 1, 555.00);
INSERT INTO `watch` VALUES (8, '2018-07-01 21:43:47', 'Sat #8', 'opis za sat #8', 5, 21000.00);
INSERT INTO `watch` VALUES (9, '2018-07-04 21:44:33', 'Sat #9', 'opis za sat #9', 4, 7500.00);
INSERT INTO `watch` VALUES (10, '2018-07-04 21:44:44', 'Sat #10', 'opis za sat #110', 2, 27999.00);

-- ----------------------------
-- Table structure for watch_category
-- ----------------------------
DROP TABLE IF EXISTS `watch_category`;
CREATE TABLE `watch_category`  (
  `watch_category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `watch_id` int(11) UNSIGNED NOT NULL,
  `category_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`watch_category_id`) USING BTREE,
  UNIQUE INDEX `uq_watch_category_watch_id_category_id`(`watch_id`, `category_id`) USING BTREE,
  INDEX `fk_watch_category_category_id`(`category_id`) USING BTREE,
  INDEX `fk_watch_category_watch_id`(`watch_id`) USING BTREE,
  CONSTRAINT `fk_watch_category_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_watch_category_watch_id` FOREIGN KEY (`watch_id`) REFERENCES `watch` (`watch_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of watch_category
-- ----------------------------
INSERT INTO `watch_category` VALUES (1, '2018-05-30 22:42:12', 1, 1);
INSERT INTO `watch_category` VALUES (2, '2018-07-04 05:10:05', 2, 1);
INSERT INTO `watch_category` VALUES (3, '2018-07-04 06:21:03', 3, 1);
INSERT INTO `watch_category` VALUES (4, '2018-07-04 22:54:22', 4, 1);
INSERT INTO `watch_category` VALUES (5, '2018-07-04 22:54:28', 5, 1);
INSERT INTO `watch_category` VALUES (6, '2018-07-04 22:54:32', 6, 1);
INSERT INTO `watch_category` VALUES (7, '2018-07-04 22:55:12', 7, 1);
INSERT INTO `watch_category` VALUES (8, '2018-07-04 22:55:16', 8, 1);
INSERT INTO `watch_category` VALUES (9, '2018-07-04 22:55:19', 9, 1);
INSERT INTO `watch_category` VALUES (10, '2018-07-04 22:55:25', 10, 1);

-- ----------------------------
-- Table structure for watch_feature
-- ----------------------------
DROP TABLE IF EXISTS `watch_feature`;
CREATE TABLE `watch_feature`  (
  `watch_feature_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `watch_id` int(11) UNSIGNED NOT NULL,
  `feature_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`watch_feature_id`) USING BTREE,
  UNIQUE INDEX `uq_watch_feature_watch_id_feature_id`(`watch_id`, `feature_id`) USING BTREE,
  INDEX `fk_watch_feature_watch_id`(`watch_id`) USING BTREE,
  INDEX `fk_watch_feature_feature_id`(`feature_id`) USING BTREE,
  CONSTRAINT `fk_watch_feature_feature_id` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`feature_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_watch_feature_watch_id` FOREIGN KEY (`watch_id`) REFERENCES `watch` (`watch_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of watch_feature
-- ----------------------------
INSERT INTO `watch_feature` VALUES (1, '2018-07-04 05:09:42', 1, 1);
INSERT INTO `watch_feature` VALUES (2, '2018-07-04 05:09:51', 2, 2);
INSERT INTO `watch_feature` VALUES (3, '2018-07-04 06:21:03', 3, 3);
INSERT INTO `watch_feature` VALUES (4, '2018-07-04 22:59:02', 4, 4);
INSERT INTO `watch_feature` VALUES (5, '2018-07-04 22:59:06', 5, 5);
INSERT INTO `watch_feature` VALUES (6, '2018-07-04 22:59:46', 6, 6);
INSERT INTO `watch_feature` VALUES (7, '2018-07-04 22:59:49', 7, 7);
INSERT INTO `watch_feature` VALUES (8, '2018-07-04 22:59:52', 8, 8);
INSERT INTO `watch_feature` VALUES (9, '2018-07-04 22:59:55', 9, 9);
INSERT INTO `watch_feature` VALUES (10, '2018-07-04 23:01:11', 10, 10);

SET FOREIGN_KEY_CHECKS = 1;
